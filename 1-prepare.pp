vcsrepo { 'puppet_repo':
  ensure   => 'bare',
  provider => 'git',
  path     => '/var/lib/puppet/puppet-code.git',
  user     => 'puppet',
  group    => 'puppet',
  umask    => '0022',
}

host { 'puppet':
  host_aliases => ['puppet-dev.skink'],
  ip => '127.0.0.1',
}
