node 'puppet-dev.skink' {
  include stdlib
  include rbac
  include yapgp
  include apt
  include tails::profile::puppet::master
}
