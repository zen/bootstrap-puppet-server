CWD := $(patsubst %/,%,$(dir $(abspath $(lastword $(MAKEFILE_LIST)))))
PUPPET_CODE_DIR := $(CWD)/puppet-code

reset-repo:
	git submodule update --init
	git -C $(PUPPET_CODE_DIR)/ checkout -f
	git -C $(PUPPET_CODE_DIR)/ submodule foreach --recursive git reset --hard
	git -C $(PUPPET_CODE_DIR)/ apply $(CWD)/2-no-eyaml.patch

.ONESHELL:
stage-0:
	export DEBIAN_FRONTEND=noninteractive
	export APT_LISTCHANGES_FRONTEND=cat
	apt-get -y purge \
		puppetserver \
		puppetdb \
		puppet-agent
	apt-get -y purge postgresql-15
	rm -rf \
		/var/lib/puppet \
		/etc/puppet \
		/var/cache/puppet \
		/var/lib/puppetdb \
		/etc/puppetdb \
		/var/lib/postgresql \
		/etc/postgresql

.ONESHELL:
stage-1: reset-repo
	export DEBIAN_FRONTEND=noninteractive
	export APT_LISTCHANGES_FRONTEND=cat
	apt-get -y install hiera-eyaml ruby-scanf
	apt-get -y install puppet-agent postgresql-15
	apt-get -y -o Dpkg::Options::="--force-confnew" install puppetserver puppetdb
	chmod 755 /var/lib/puppet
	install -d -g puppet -o puppet \
		/var/lib/puppet/.gnupg \
		/var/lib/puppet/.ssh \
		/var/lib/puppet/server_data/reports
	for i in $$( git -C ./puppet-code/ submodule | awk '{print $$2}' ); do \
		if [ "$$i" != "modules/augeasproviders_core" -a "$$i" != "modules/augeasproviders_ssh" ]; then RECURSE="--recursive"; else RECURSE=""; fi; \
		git -C ./puppet-code/ submodule update --init $$RECURSE $$i; \
	done
	echo puppet apply \
		--detailed-exitcodes \
		--modulepath $(PUPPET_CODE_DIR)/modules \
		--hiera-config $(PUPPET_CODE_DIR)/hiera.yaml \
		--codedir $(PUPPET_CODE_DIR) \
		./1-prepare.pp
	echo "listen_addresses = '*'" >> /etc/postgresql/15/main/postgresql.conf
	systemctl restart postgresql
	systemctl start puppetdb


stage-2: reset-repo
	git -C $(PUPPET_CODE_DIR)/modules/tails/ apply $(CWD)/2-no-puppetdb.patch
	puppet apply \
		--detailed-exitcodes \
		--modulepath $(PUPPET_CODE_DIR)/modules \
		--hiera-config $(PUPPET_CODE_DIR)/hiera.yaml \
		--codedir $(PUPPET_CODE_DIR) \
		./2-node-basic.pp

stage-3:
	cp /var/lib/puppet/ssl/ca/ca_crt.pem /etc/puppetdb/ca.pem;
	chgrp puppetdb /etc/puppetdb/ca.pem;
	cp /var/lib/puppet/ssl/certs/puppet-dev.skink.pem /etc/puppetdb/;
	cp /var/lib/puppet/ssl/private_keys/puppet-dev.skink.pem /etc/puppetdb/puppet-dev.skink.key;
	chgrp puppetdb /etc/puppetdb/puppet-dev.skink.key;
	cat jetty.ini > /etc/puppetdb/conf.d/jetty.ini
	systemctl restart puppetdb

stage-4: reset-repo
	rm -f /etc/puppet/puppet.conf
	apt-get -y install puppet-terminus-puppetdb
	puppet apply \
		--detailed-exitcodes \
		--modulepath $(PUPPET_CODE_DIR)/modules \
		--hiera-config $(PUPPET_CODE_DIR)/hiera.yaml \
		--codedir $(PUPPET_CODE_DIR) \
		./2-node-basic.pp

stage-5: reset-repo
	rm -f /etc/puppet/puppet.conf
	mkdir /var/lib/puppet-sshkeys
	puppet apply \
		--detailed-exitcodes \
		--modulepath $(PUPPET_CODE_DIR)/modules \
		--hiera-config $(PUPPET_CODE_DIR)/hiera.yaml \
		--codedir $(PUPPET_CODE_DIR) \
		./3-node-complete.pp

stage-6: reset-repo
	rm -rf /etc/puppet/code/environments/production/*
	cp -r $(PUPPET_CODE_DIR)/* /etc/puppet/code/environments/production/
	cp ./3-node-complete.pp /etc/puppet/code/environments/production/manifests/
	puppet agent -t --server puppet
