node 'puppet-dev.skink' {
  include tails::profile::base
  include tails::profile::network
  include tails::profile::puppet::master
  include tails::profile::puppet::enc
  include tails::profile::etckeeper
  include tails::profile::nonfs
  include tails::profile::sysadmins
  include tails::profile::rbac
  include tails::profile::apt
  include tails::profile::grub
  include tails::profile::mta
  include tails::profile::firewall
  include tails::profile::sshkeymaster
}
